package main

import (
	"encoding/json"
	"log"
	"net/http"
)

// Result represents the result from the API
type Result struct {
	Status string `json:"status"`
}

func index(w http.ResponseWriter, r *http.Request) {
	result := Result{
		Status: "ok from API",
	}

	json.NewEncoder(w).Encode(result)
}

func main() {
	http.HandleFunc("/api", index)

	err := http.ListenAndServe(":8080", nil)
	if err != http.ErrServerClosed {
		log.Fatal(err)
	}
}
